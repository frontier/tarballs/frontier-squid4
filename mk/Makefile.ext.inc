
DIST_DIR=$(PORT_ROOT)/distrib
FILES_DIR=$(PORT_ROOT)/$(PORT_NAME)/files
WORK_DIR=$(PORT_ROOT)/$(PORT_NAME)/work

PORTS_INSTALL=$(INSTALL_DIR)

all: compile

compile: config do_compile

config: patch .config

patch: unpack .common_patch .patch

.common_patch:
	$(PORT_ROOT)/mk/patching.sh $(WORK_DIR)/$(SRC) $(FILES_DIR)
	touch .common_patch

unpack: download .unpack

download:
	(if [ -f $(DIST_DIR)/$(DIST_NAME) ]; then exit 0; fi; for i in `echo $(DIST_LINKS)` ; do $(PORT_ROOT)/mk/download.sh $(DIST_DIR) $(FILES_DIR) $$i $(DIST_NAME) ; status=$$?; if [ $$status == 0 ] ; then exit 0; fi; done; echo "Can not download $(DIST_NAME). Please download it manually and put into distrib directory."; exit 1; )


clean:
	rm -rf $(WORK_DIR) .unpack .patch .config .common_patch
	(cd $(DIST_DIR); if [ -f .downloaded ]; then rm -f .downloaded $(DIST_NAME); fi)

