PORT_NAME=squid
SRC=squid-4.17
RELEASE=2

# Now always include the source tarball in the package, but code is still
#  here to download in case we want it again later
# DIST_LINKS="http://www.squid-cache.org/Versions/v4/$(SRC).tar.gz"
DIST_LINKS=""
DIST_NAME=$(SRC).tar.gz

SQUID_SUFFIX=

include ../mk/Makefile.ext.inc

.unpack:
	if [ ! -d $(WORK_DIR) ] ; then mkdir $(WORK_DIR); fi
	(cd $(WORK_DIR) && tar xzvf $(DIST_DIR)/$(DIST_NAME))
	touch .unpack

.patch:
	rm -f $(FILES_DIR)/postinstall/squid.conf.frontierdefault
	sed -e "s,@@@LIBEXEC_DIR@@@,$(INSTALL_DIR)/squid/libexec,g;s,@@@ETC_DIR@@@,$(INSTALL_DIR)/squid/etc,g;s,@@@CACHE_DIR@@@,$(INSTALL_DIR)/squid/var/cache,g;s,@@@SHARE_DIR@@@,$(INSTALL_DIR)/squid/share,g;s,@@@LIB_DIR@@@,$(INSTALL_DIR)/squid/lib,g;s,@@@VARLIB_DIR@@@,$(INSTALL_DIR)/squid/var/lib,g;s,@@@LOG_DIR@@@,$(INSTALL_DIR)/squid/var/logs,g;s,@@@RUN_DIR@@@,$(INSTALL_DIR)/squid/var/logs,g;s,@@@EFFECTIVE_USER@@@,$(FRONTIER_USER),g;s,@@@EFFECTIVE_GROUP@@@,$(FRONTIER_GROUP),g;s,@@@SQUID_SUFFIX@@@,,g" $(FILES_DIR)/postinstall/squid.conf.proto > $(FILES_DIR)/postinstall/squid.conf.frontierdefault
	if [ ! -f $(FILES_DIR)/postinstall/customize.sh ]; then sed -e "s,@@@INIT_SCRIPT@@@,$(INSTALL_DIR)/utils/bin/fn-local-squid.sh,g;s,@@@CACHE_MEM@@@,$(FRONTIER_CACHE_MEM),g;s,@@@CACHE_DIR_SIZE@@@,$(FRONTIER_CACHE_DIR_SIZE),g;s,@@@NET_LOCAL@@@,$(FRONTIER_NET_LOCAL),g" $(FILES_DIR)/postinstall/customize.sh.proto > $(FILES_DIR)/postinstall/customize.sh; fi
	chmod 755 $(FILES_DIR)/postinstall/customize.sh
	touch .patch
	
.config:
        # --with-included-ltdl is needed on sl6 with devtoolset-2-toolchain
        #   which is needed there for gcc 4.8 and c++11
	(cd $(WORK_DIR)/$(SRC) && \
	sed -i "s/VERSION='\(.*\)'/VERSION=frontier-$(SRC)-$(RELEASE)/" configure && \
	./configure --prefix=$(PORTS_INSTALL)/$(PORT_NAME) --disable-wccp --enable-snmp --disable-ident-lookups --enable-storeio="ufs aufs diskd rock" --with-large-files --with-included-ltdl --disable-esi)
 
	touch .config

do_compile:
	(cd $(WORK_DIR)/$(SRC) && $(MAKE) -j 2)

do_install:
	mkdir -p $(PORTS_INSTALL)/$(PORT_NAME)/var/cache
	(DESTD=$(PORTS_INSTALL)/$(PORT_NAME)/etc; if [ ! -f $$DESTD/squid.conf ]; then mkdir -p $$DESTD; echo "# placeholder to be overwritten by fn-local-squid.sh but not make install" >$$DESTD/squid.conf; chmod 444 $$DESTD/squid.conf; fi)
	(cd $(WORK_DIR)/$(SRC) && $(MAKE) install)
	(DESTD=$(PORTS_INSTALL)/$(PORT_NAME)/etc; cd $(FILES_DIR)/postinstall && cp -f squid.conf.frontierdefault customhelps.awk $$DESTD && if [ ! -f $$DESTD/customize.sh ]; then cp customize.sh $$DESTD; fi)
	$(PORTS_INSTALL)/utils/bin/fn-local-squid.sh rebuildconf

proto_install:
	install -d $(INSTALL_DIR)/etc/squid$(SQUID_SUFFIX)
	install -m 644 files/postinstall/squid.conf.proto $(INSTALL_DIR)/etc/squid$(SQUID_SUFFIX)/squid.conf.proto
	install -m 644 files/postinstall/customize.sh.proto $(INSTALL_DIR)/etc/squid$(SQUID_SUFFIX)/customize.sh.proto
	install -m 644 files/postinstall/customhelps.awk $(INSTALL_DIR)/etc/squid$(SQUID_SUFFIX)/customhelps.awk
	
#the main clean is in Makefile.ext.inc
clean: extraclean
extraclean:
	rm -f files/postinstall/squid.conf.frontierdefault
